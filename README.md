This script keeps selected files synchronized between a git repo on a
Linux host and another device, such as a mobile phone. It is meant to
run as a cron job on the Linux host and connects to the mobile device
using any protocol supported by [curl](https://curl.haxx.se/), such as
[SCP](https://en.wikipedia.org/wiki/Secure_copy) or
[FTP](https://en.wikipedia.org/wiki/File_Transfer_Protocol).

The script performs the following steps:

1. Transfers files from mobile device to host.
2. Commits the files to a branch dedicated to track changes from the
   mobile device.
3. Attempts to merge the mobile tracking branch with the main branch
   (normally the `master` branch).
4. If merge succeeded without conflicts, the files are transferred
   back to the mobile device. If there are conflicts, the script exits
   with an error so that user can deal with the conflicts manually. If
   cron is setup properly, the user should receive an email when this
   happens.

## Setup

Copy the file `mobile-git-sync.conf-template` to `~/.mobile-git-sync`
and modify it according to your needs. If the files are not already in
place on the mobile device, you should run the script once with the
`-u`/`--upload-only` option to get them there. Next, it is strongly
recommended to run the script manually once with the `-v`/`--verbose`
option. You may then run it without arguments as a cron job.

## Authorization

It is preferable to keep authorization for the transfers outside the
settings for this script. If you use FTP, add your credentials to your
[.netrc](https://www.gnu.org/software/inetutils/manual/html_node/The-_002enetrc-file.html)
file. If you use SCP, use SSH keys.

## Dealing with merge conflicts

If the script fails due to merge conflicts, you have to dive into the
repo and fix them manually. Commit the fixed merge yourself and then
run the script manually with the `-u`/`--upload-only` option.

## Usage for Org mode files

This script was written in order to [synchronize Org mode notes
between Emacs and a mobile phone](https://pantarei.xyz/posts/sync-org-mode-with-mobile/), but could be used to synchronize any type of files.
